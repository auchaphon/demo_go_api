package api

import (
	"api/controller"
	"fmt"

	"github.com/labstack/echo"
)

func NewServer(port int) {
	e := echo.New()
	e.GET("/", controller.Hello)
	e.GET("/users", controller.GetUsers)
	e.Start(fmt.Sprintf(":%v", port))
}
